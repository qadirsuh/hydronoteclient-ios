//
//  ContentView.swift
//  HydroNote
//
//  Created by Qadir Hussain on 13/07/2020.
//  Copyright © 2020 Qadir Hussain. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @State var toggleValue = false
    
    var body: some View {
        // Text("Hello, World! sample check HI there")
        NavigationView{
            
            Form{
                Toggle(isOn: $toggleValue){
                    Text("Sample check")
                }
        }
        }.navigationBarTitle("Settings")

    }
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
